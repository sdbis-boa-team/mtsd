### General Business Rules

##### Business Rule 1:
Before an Interview can happen
A Test has to be taken by the Candidate

##### Business Rule 2:
The Interview's Result can be given by a different interviewer than the one who has initially held the Interview

##### Business Rule 3:
A Test can be taken or not by the Candidate (eg. when they're not interested anymore)

##### Business Rule 4:
A Candidate can receive more than one Test

##### Business Rule 5:
An interviewer can hold more than one Interview

##### Business Rule 6:
A Question can be added in multiple Tests

##### Business Rule 7:
A candidate will receive the result after the interview/test through e-mail or by phone

### Candidates Section Business Rules

##### BR1: add a new candidate
  * When adding a candidate, the work_experience field is not mandatory (if it's missing it'll be `0`)
  * It's mandatory that a candidate offers all those needed information about them to be taken in consideration (full name, address, phone etc.)
  * The location where the candidate gives the interview is either a physical building, either through online platforms (Skype, Teams etc).

##### BR2: edit data of a candidate
  * As an interviewer, at the final of the interview
I can change the data of a candidate
  * As an interviewer, if I did a mistake in filling the candidate's data form, 
I can edit the form
  * As an interviewer, if I want to edit the candidate's data form by 
adding more details/ update information, i can do it by editing the form

##### BR3: deleting a candidate 
  * As an interviewer, at the final of the interview 
I can delete the candidates that didn't attend the interview or test
  * As an interviewer, I can delete a candidate, if they canceled the interview or test
  * As an interviewer, I can delete a candidate, if they no longer respond to our email/phone invitation to give an interview
  * After the interview/test take place, but the candidate didn't receive a favorable result, they can be stored in the database until manual deletion by an interviewer
