# Interviews Management Application
* ### REST API
First, replace the datasource (PostgreSQL database) url, user and password in the `src/main/resources/application.properties` file.

Then build the application (located in the `spring-boot-app` folder), created upon the Spring Boot framework (Java 15) and PostgreSQL.

The API is exposed at http://localhost:8080/api/v1 and is documented with Swagger at http://localhost:8080/swagger-ui/index.htm.

* ### The frontend application
Is located in the `react-app` folder.

Build and run the application using `npm start` and you whould see it at `http://localhost:3000`.
