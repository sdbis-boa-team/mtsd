package com.mtsd.interviews.service;

import org.springframework.stereotype.Service;
import com.mtsd.interviews.model.*;
import com.mtsd.interviews.repository.TestRepository;
import com.mtsd.interviews.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.mtsd.interviews.exception.ResourceNotFoundException;

@Service
public class CandidateTestService {
    private final TestRepository testRepository;
    private final CandidateRepository candidateRepository;

    @Autowired
    public CandidateTestService(TestRepository testRepository, CandidateRepository candidateRepository) {
        this.testRepository = testRepository;
        this.candidateRepository = candidateRepository;
    }

    private void coupleCandidateWithTest(Candidate candidate, Test test) throws ResourceNotFoundException {
        Test testToTake = this.testRepository.findById(test.getId())
                .orElseThrow(() -> new com.mtsd.interviews.exception.ResourceNotFoundException("Test not found!"));
        Candidate candidateTakingTest = this.candidateRepository.findById(candidate.getId())
                .orElseThrow(() -> new com.mtsd.interviews.exception.ResourceNotFoundException("Candidate not found!"));
        candidateTakingTest.setTest(testToTake);
        candidateRepository.save(candidateTakingTest);
    }

    public Test getTestForCandidate(Candidate candidate) throws ResourceNotFoundException {
        Candidate candidateTakingTest = this.candidateRepository.findById(candidate.getId())
                .orElseThrow(() -> new com.mtsd.interviews.exception.ResourceNotFoundException("Candidate not found!"));
        return candidateTakingTest.getTest();
    }
}
