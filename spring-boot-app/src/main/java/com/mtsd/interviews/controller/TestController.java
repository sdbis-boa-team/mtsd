package com.mtsd.interviews.controller;

import com.mtsd.interviews.exception.ResourceNotFoundException;
import com.mtsd.interviews.model.Test;
import com.mtsd.interviews.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class TestController {

    private final TestRepository testRepository;

    @Autowired
    public TestController(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @GetMapping("/tests")
    public List<Test> getAllTests() {
        return testRepository.findAll();
    }

    @GetMapping("/tests/{id}")
    public ResponseEntity<Test> getTestById(@PathVariable(value = "id") Long testId)
            throws ResourceNotFoundException {
        Test test = testRepository.findById(testId)
                .orElseThrow(() -> new ResourceNotFoundException("Test not found for this id: " + testId));

        return ResponseEntity.ok().body(test);
    }

    @PostMapping("/tests")
    public Test createTest(@Valid @RequestBody Test test) {
        return testRepository.save(test);
    }

    @PutMapping("/tests/{id}")
    public ResponseEntity<Test> updateTest(
            @PathVariable(value = "id") Long testId,
            @Valid @RequestBody Test testDetails
    ) throws ResourceNotFoundException {
        Test test = testRepository.findById(testId)
                .orElseThrow(() -> new ResourceNotFoundException("Test not found for this id: " + testId));

        test.setName(testDetails.getName());
        final Test updatedTest = testRepository.save(test);

        return ResponseEntity.ok(updatedTest);
    }

    @DeleteMapping("/tests/{id}")
    public Map<String, Boolean> deleteTest(@PathVariable(value = "id") Long testId)
            throws ResourceNotFoundException {
        Test test = testRepository.findById(testId)
                .orElseThrow(() -> new ResourceNotFoundException("Test not found for this id: " + testId));

        testRepository.delete(test);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }
}
