package com.mtsd.interviews.controller;

import com.mtsd.interviews.exception.ResourceNotFoundException;
import com.mtsd.interviews.model.Question;
import com.mtsd.interviews.repository.QuestionRepository;
import com.mtsd.interviews.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class QuestionController {

    private final QuestionRepository questionRepository;

    private final TestRepository testRepository;

    @Autowired
    public QuestionController(QuestionRepository questionRepository, TestRepository testRepository) {
        this.questionRepository = questionRepository;
        this.testRepository = testRepository;
    }

    @GetMapping("/tests/{testId}/questions")
    public List<Question> getQuestionsByTestId(@PathVariable Long testId) throws ResourceNotFoundException {

        if (!testRepository.existsById(testId)) {
            throw new ResourceNotFoundException("Test not found!");
        }

        return questionRepository.findByTestId(testId);
    }

    @PostMapping("/tests/{testId}/questions")
    public Question createQuestion(
            @PathVariable Long testId,
            @Valid @RequestBody Question question
    ) throws ResourceNotFoundException {
        return testRepository.findById(testId)
                .map(test -> {
                    question.setTest(test);
                    return questionRepository.save(question);
                }).orElseThrow(() -> new ResourceNotFoundException("Test not found!"));
    }

    @PutMapping("/tests/{testId}/questions/{questionId}")
    public ResponseEntity<Question> updateQuestion(
            @PathVariable Long testId,
            @PathVariable Long questionId,
            @Valid @RequestBody Question questionUpdated
    ) throws ResourceNotFoundException {
        if (!testRepository.existsById(testId)) {
            throw new ResourceNotFoundException("Test not found!");
        }

        return questionRepository.findById(questionId)
                .map(question -> {
                    question.setText(questionUpdated.getText());
                    question.setUserAnswer(questionUpdated.getUserAnswer());
                    question.setCorrectAnswer(questionUpdated.getCorrectAnswer());
                    question.setScore(questionUpdated.getScore());
                    return ResponseEntity.ok(questionRepository.save(question));
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found!"));
    }

    @DeleteMapping("/tests/{testId}/questions/{questionId}")
    public Map<String, Boolean> deleteQuestion(
            @PathVariable Long testId,
            @PathVariable Long questionId
    ) throws ResourceNotFoundException {
        if (!testRepository.existsById(testId)) {
            throw new ResourceNotFoundException("Test not found!");
        }

        return questionRepository.findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    Map<String, Boolean> response = new HashMap<>();
                    response.put("deleted", Boolean.TRUE);
                    return response;
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found!"));
    }
}
