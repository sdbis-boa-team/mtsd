package com.mtsd.interviews.controller;

import com.mtsd.interviews.exception.ResourceNotFoundException;
import com.mtsd.interviews.model.Interviewer;
import com.mtsd.interviews.repository.InterviewerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:3000")
public class InterviewerController {

    private final InterviewerRepository interviewerRepository;

    @Autowired
    public InterviewerController(InterviewerRepository interviewerRepository) {
        this.interviewerRepository = interviewerRepository;
    }

    @GetMapping("/interviewers")
    public List<Interviewer> getAllInterviewers() {
        return interviewerRepository.findAll();
    }

    @GetMapping("/interviewers/{id}")
    public ResponseEntity<Interviewer> getInterviewerById(@PathVariable(value = "id") Long interviewerId)
            throws ResourceNotFoundException {
        Interviewer interviewer = interviewerRepository.findById(interviewerId)
                .orElseThrow(() -> new ResourceNotFoundException("Interviewer not found for this id: " + interviewerId));

        return ResponseEntity.ok().body(interviewer);
    }

    @PostMapping("/interviewers")
    public Interviewer createInterviewer(@Valid @RequestBody Interviewer interviewer) {
        return interviewerRepository.save(interviewer);
    }

    @PutMapping("/interviewers/{id}")
    public ResponseEntity<Interviewer> updateInterviewer(
            @PathVariable(value = "id") Long interviewerId,
            @Valid @RequestBody Interviewer interviewerDetails
    ) throws ResourceNotFoundException {
        Interviewer interviewer = interviewerRepository.findById(interviewerId)
                .orElseThrow(() -> new ResourceNotFoundException("Interviewer not found for this id: " + interviewerId));

        interviewer.setFullName(interviewerDetails.getFullName());
        interviewer.setEmail(interviewerDetails.getEmail());
        interviewer.setDepartment(interviewerDetails.getDepartment());
        final Interviewer updatedInterviewer = interviewerRepository.save(interviewer);

        return ResponseEntity.ok(updatedInterviewer);
    }

    @DeleteMapping("/interviewers/{id}")
    public Map<String, Boolean> deleteInterviewer(@PathVariable(value = "id") Long interviewerId)
            throws ResourceNotFoundException {
        Interviewer interviewer = interviewerRepository.findById(interviewerId)
                .orElseThrow(() -> new ResourceNotFoundException("Interviewer not found for this id: " + interviewerId));

        interviewerRepository.delete(interviewer);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }
}
