package com.mtsd.interviews.controller;

import com.mtsd.interviews.exception.ResourceNotFoundException;
import com.mtsd.interviews.model.Result;
import com.mtsd.interviews.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class ResultController {

    private final ResultRepository resultRepository;

    @Autowired
    public ResultController(ResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    @GetMapping("/results")
    public List<Result> getAllResults() {
        return resultRepository.findAll();
    }

    @GetMapping("/results/{id}")
    public ResponseEntity<Result> getResultById(@PathVariable(value = "id") Long resultId)
            throws ResourceNotFoundException {
        Result result = resultRepository.findById(resultId)
                .orElseThrow(() -> new ResourceNotFoundException("Result not found for this id: " + resultId));

        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/results")
    public Result createResult(@Valid @RequestBody Result result) {
        return resultRepository.save(result);
    }

    @PutMapping("/results/{id}")
    public ResponseEntity<Result> updateResult(
            @PathVariable(value = "id") Long resultId,
            @Valid @RequestBody Result resultDetails
    ) throws ResourceNotFoundException {
        Result result = resultRepository.findById(resultId)
                .orElseThrow(() -> new ResourceNotFoundException("Result not found for this id: " + resultId));

        result.setDetails(resultDetails.getDetails());
        result.setResult(resultDetails.getResult());
        final Result updatedResult = resultRepository.save(result);

        return ResponseEntity.ok(updatedResult);
    }

    @DeleteMapping("/results/{id}")
    public Map<String, Boolean> deleteResult(@PathVariable(value = "id") Long resultId)
            throws ResourceNotFoundException {
        Result result = resultRepository.findById(resultId)
                .orElseThrow(() -> new ResourceNotFoundException("Result not found for this id: " + resultId));

        resultRepository.delete(result);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }
}
