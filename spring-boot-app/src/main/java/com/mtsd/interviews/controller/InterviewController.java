package com.mtsd.interviews.controller;

import com.mtsd.interviews.exception.ResourceNotFoundException;
import com.mtsd.interviews.model.Interview;
import com.mtsd.interviews.repository.InterviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class InterviewController {

    private final InterviewRepository interviewRepository;

    @Autowired
    public InterviewController(InterviewRepository interviewRepository) {
        this.interviewRepository = interviewRepository;
    }

    @GetMapping("/interviews")
    public List<Interview> getAllInterviews() {
        return interviewRepository.findAll();
    }

    @GetMapping("/interviews/{id}")
    public ResponseEntity<Interview> getInterviewById(@PathVariable(value = "id") Long interviewId)
            throws ResourceNotFoundException {
        Interview interview = interviewRepository.findById(interviewId)
                .orElseThrow(() -> new ResourceNotFoundException("Interview not found for this id: " + interviewId));

        return ResponseEntity.ok().body(interview);
    }

    @PostMapping("/interviews")
    public Interview createInterview(@Valid @RequestBody Interview interview) {
        return interviewRepository.save(interview);
    }

    @PutMapping("/interviews/{id}")
    public ResponseEntity<Interview> updateInterview(
            @PathVariable(value = "id") Long interviewId,
            @Valid @RequestBody Interview interviewDetails
    ) throws ResourceNotFoundException {
        Interview interview = interviewRepository.findById(interviewId)
                .orElseThrow(() -> new ResourceNotFoundException("Interview not found for this id: " + interviewId));

        interview.setDuration(interviewDetails.getDuration());
        interview.setLocation(interviewDetails.getLocation());
        interview.setScheduledDate(interviewDetails.getScheduledDate());
        interview.setType(interviewDetails.getType());
        final Interview updatedInterview = interviewRepository.save(interview);

        return ResponseEntity.ok(updatedInterview);
    }

    @DeleteMapping("/interviews/{id}")
    public Map<String, Boolean> deleteInterview(@PathVariable(value = "id") Long interviewId)
            throws ResourceNotFoundException {
        Interview interview = interviewRepository.findById(interviewId)
                .orElseThrow(() -> new ResourceNotFoundException("Interview not found for this id: " + interviewId));

        interviewRepository.delete(interview);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }
}
