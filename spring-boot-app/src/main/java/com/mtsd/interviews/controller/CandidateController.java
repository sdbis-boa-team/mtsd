package com.mtsd.interviews.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import com.mtsd.interviews.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.mtsd.interviews.exception.ResourceNotFoundException;
import com.mtsd.interviews.model.Candidate;
import com.mtsd.interviews.repository.CandidateRepository;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:3000")
public class CandidateController {

    private final CandidateRepository candidateRepository;

    private final TestRepository testRepository;

    @Autowired
    public CandidateController(CandidateRepository candidateRepository, TestRepository testRepository) {
        this.candidateRepository = candidateRepository;
        this.testRepository = testRepository;
    }

    @GetMapping("/candidates")
    public List<Candidate> getAllCandidates() {
        return candidateRepository.findAll();
    }

    @GetMapping("/candidates/{id}")
    public ResponseEntity<Candidate> getCandidateById(@PathVariable(value = "id") Long candidateId)
            throws ResourceNotFoundException {
        Candidate candidate = candidateRepository.findById(candidateId)
                .orElseThrow(() -> new ResourceNotFoundException("Candidate not found for this id: " + candidateId));

        return ResponseEntity.ok().body(candidate);
    }

    @PostMapping("/candidates")
    public Candidate createCandidate(@Valid @RequestBody Candidate candidate) {
        return candidateRepository.save(candidate);
    }

    @PutMapping("/candidates/{id}")
    public ResponseEntity<Candidate> updateCandidate(
            @PathVariable(value = "id") Long candidateId,
            @Valid @RequestBody Candidate candidateDetails
    ) throws ResourceNotFoundException {
        Candidate candidate = candidateRepository.findById(candidateId)
                .orElseThrow(() -> new ResourceNotFoundException("Candidate not found for this id: " + candidateId));

        candidate.setFullName(candidateDetails.getFullName());
        candidate.setGender(candidateDetails.getGender());
        candidate.setBirthDate(candidateDetails.getBirthDate());
        candidate.setHomeAddress(candidateDetails.getHomeAddress());
        candidate.setEmail(candidateDetails.getEmail());
        candidate.setMobileNumber(candidateDetails.getMobileNumber());
        candidate.setWorkExperience(candidateDetails.getWorkExperience());
        candidate.setStudies(candidateDetails.getStudies());
        candidate.setMainSkills(candidateDetails.getMainSkills());
        final Candidate updatedCandidate = candidateRepository.save(candidate);

        return ResponseEntity.ok(updatedCandidate);
    }

    @DeleteMapping("/candidates/{id}")
    public Map<String, Boolean> deleteCandidate(@PathVariable(value = "id") Long candidateId)
            throws ResourceNotFoundException {
        Candidate candidate = candidateRepository.findById(candidateId)
                .orElseThrow(() -> new ResourceNotFoundException("Candidate not found for this id: " + candidateId));

        candidateRepository.delete(candidate);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }

    @GetMapping("/tests/{testId}/candidates")
    public List<Candidate> getTestCandidates(@PathVariable Long testId) {
        return candidateRepository.findByTestId(testId);
    }

    @PostMapping("/tests/{testId}/candidates")
    public Candidate addCandidateToTest(
            @PathVariable Long testId,
            @RequestBody Map<String, String> json
    ) throws ResourceNotFoundException {
        Long candidateId = Long.parseLong(json.get("candidateId"));
        Candidate candidate = candidateRepository.findById(candidateId)
                .orElseThrow(() -> new ResourceNotFoundException("Candidate not found for this id: " + candidateId));

        return testRepository.findById(testId)
                .map(test -> {
                    candidate.setTest(test);
                    return candidateRepository.save(candidate);
                }).orElseThrow(() -> new ResourceNotFoundException("Test not found!"));
    }
}
