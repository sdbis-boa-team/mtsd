package com.mtsd.interviews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.mtsd.interviews.repository")
@EnableJpaAuditing
public class InterviewsManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterviewsManagementApplication.class, args);
    }

}
