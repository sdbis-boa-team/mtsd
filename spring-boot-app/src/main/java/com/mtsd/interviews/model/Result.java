package com.mtsd.interviews.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "result")
public class Result {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "result")
    @JsonIgnore
    private Interview interview;

    @OneToOne(mappedBy = "result")
    @JsonIgnore
    private Candidate candidate;

    @OneToOne(mappedBy = "result")
    @JsonIgnore
    private Interviewer interviewer;

    @Column(name = "details", nullable = false)
    private String details;

    @Column(name = "result", nullable = false)
    private String result;

    public Result(Long id, String details, String result) {
        this.id = id;
        this.details = details;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Interview getInterview() {
        return interview;
    }

    public void setInterview(Interview interview) {
        this.interview = interview;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Interviewer getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(Interviewer interviewer) {
        this.interviewer = interviewer;
    }
}
