package com.mtsd.interviews.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "question")
public class Question extends AuditedModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "test_id")
    @JsonIgnore
    private Test test;

    @Column(name = "text", nullable = false)
    private String text;

    //TODO: once we have an user answer, how can we have another one from some other user on another test?
    // (a question will have answers from different candidates)
    // Possible solution: a new QuestionAnswer entity containing the questionId, answer and candidateId
    @Column(name = "user_answer", nullable = false)
    private String userAnswer;

    @Column(name = "correct_answer", nullable = false)
    private String correctAnswer;

    @Column(name = "score", nullable = false)
    private Float score;

    public Question() {
    }

    public Question(Long id, String text, String userAnswer, String correctAnswer, Float score) {
        this.id = id;
        this.text = text;
        this.userAnswer = userAnswer;
        this.correctAnswer = correctAnswer;
        this.score = score;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Test getTest() {
        return this.test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
