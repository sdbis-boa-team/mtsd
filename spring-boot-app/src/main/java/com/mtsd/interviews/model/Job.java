package com.mtsd.interviews.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "job")
public class Job extends AuditedModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "work_experience_needed", nullable = false)
    private Integer workExperienceNeeded;

    @Column(name = "max_budget", nullable = false)
    private Integer maxBudget;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "contract_period", nullable = false)
    private Float contractPeriod;

    @OneToMany(mappedBy = "job")
    @JsonIgnore
    private List<Interview> interviews;

    public Job(
            Long id,
            String name,
            String description,
            Integer workExperienceNeeded,
            Integer maxBudget,
            String location,
            Float contractPeriod
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.workExperienceNeeded = workExperienceNeeded;
        this.maxBudget = maxBudget;
        this.location = location;
        this.contractPeriod = contractPeriod;
    }

    public Job() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWorkExperienceNeeded() {
        return workExperienceNeeded;
    }

    public void setWorkExperienceNeeded(Integer workExperienceNeeded) {
        this.workExperienceNeeded = workExperienceNeeded;
    }

    public Integer getMaxBudget() {
        return maxBudget;
    }

    public void setMaxBudget(Integer maxBudget) {
        this.maxBudget = maxBudget;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Float getContractPeriod() {
        return contractPeriod;
    }

    public void setContractPeriod(Float contractPeriod) {
        this.contractPeriod = contractPeriod;
    }

    public List<Interview> getInterviews() {
        return interviews;
    }

    public void setInterviews(List<Interview> interviews) {
        this.interviews = interviews;
    }
}
