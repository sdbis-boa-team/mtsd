package com.mtsd.interviews.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "interview")
public class Interview extends AuditedModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "test_id")
    @JsonIgnore
    private Test test;

    @ManyToOne
    @JoinColumn(name = "interviewer_id")
    @JsonIgnore
    private Interviewer interviewer;

    @ManyToOne
    @JoinColumn(name = "job_id")
    @JsonIgnore
    private Job job;

    @OneToOne
    @JoinColumn(name = "result_id")
    @JsonIgnore
    private Result result;

    @Column(name = "scheduled_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduledDate;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "duration", nullable = false)
    private Float duration;

    @Column(name = "location", nullable = false)
    private String location;

    public Interview(Long id, Date scheduledDate, String type, Float duration, String location) {
        this.id = id;
        this.scheduledDate = scheduledDate;
        this.type = type;
        this.duration = duration;
        this.location = location;
    }

    public Interview() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public Interviewer getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(Interviewer interviewer) {
        this.interviewer = interviewer;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Date getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(Date scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
