import React from 'react';

class ContactPage extends React.Component {
    render() {
        return (
            <div className="mt-3">
                <h3>You can easily find us on <a href="https://www.facebook.com/interviews.management/"><img
                    src="facebook-icon.png" alt="Facebook"/></a><a
                    href="https://www.instagram.com/interviews.management/"><img src="instagram-icon.png"
                                                                                 alt="Instagram"/></a><a
                    href="https://www.twitter.com/interviews.management/"><img src="twitter-icon.png"
                                                                               alt="Twitter"/></a></h3>
                <p>
                    We reply as quickly as possible to our e-mail address: <a
                    href="mailto:interviews.management@gmail.com">interviews.management@gmail.com</a>
                </p>
                <p>You can also use the form below!</p>
                <form>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">First name</label>
                        <input className="form-control" type="text" placeholder="Your first name here..."
                               aria-label="First name"/>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Last name</label>
                        <input className="form-control" type="text" placeholder="Your last name here..."
                               aria-label="First name"/>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1"
                               placeholder="Where can we answer your question?"
                               aria-describedby="emailHelp"/>
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.
                        </div>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlTextarea1" className="form-label">Message</label>
                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"
                                  placeholder="Tell us how can we help"/>
                    </div>
                    <button type="submit" className="btn btn-primary">Send</button>
                </form>
            </div>
        );
    }
}

export default ContactPage;
