import React from 'react';

class AddInterviewerModal extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            fullName: "",
            gender: "",
            birthDate: "",
            homeAddress: "",
            email: "",
            mobileNumber: "",
            workExperience: 0,
            studies: "",
            mainSkills: ""
        }
    }

    handleSubmit(e) {
        fetch('http://localhost:8080/api/v1/interviewers/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                this.props.feedbackHandler('Successfully added interviewer ' + this.state.fullName, 'success');
            })
            .catch((error) => {
                console.error('Error:', error);
                this.props.feedbackHandler('Error while trying to add interviewer ' + this.state.fullName, 'error');
            });
        e.preventDefault();
        this.dismissModal();
    }

    handleChange = (e) => {
        const id = e.target.id;
        const value = e.target.value;
        this.setState({
            [id]: value
        });
    }

    dismissModal = () => {
        this.props.toggle();
    }

    render() {
        return (
            <div>
                {
                    <div
                        className={`modal fade addInterviewerModal ${this.props.showModal ? 'show' : ''}`}
                        style={{
                            display: `${this.props.showModal ? 'block' : 'none'}`,
                            backgroundColor: `${this.props.showModal ? 'rgba(0,0,0,0.5)' : ''}`,
                        }} tabIndex="-1" id="addInterviewerModal">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">Add a new interviewer</h5>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"
                                            onClick={this.dismissModal}/>
                                </div>
                                <div className="modal-body">
                                    <form onSubmit={this.handleSubmit}>
                                        <div className="row mb-3">
                                            <label htmlFor="fullName" className="col-sm-2 col-form-label">Name</label>
                                            <div className="col-sm-10">
                                                <input type="text" className="form-control" id="fullName"
                                                       value={this.state.fullName}
                                                       onChange={this.handleChange}
                                                       required
                                                />
                                            </div>
                                        </div>
                                        <div className="row mb-3">
                                            <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                                            <div className="col-sm-10">
                                                <input type="email" className="form-control" id="email"
                                                       value={this.state.email}
                                                       onChange={this.handleChange}
                                                       required
                                                />
                                            </div>
                                        </div>
                                        <div className="row mb-3">
                                            <label htmlFor="department" className="col-sm-2 col-form-label">Department</label>
                                            <div className="col-sm-10">
                                                <select className="form-select" aria-label="Department select"
                                                        value={this.state.department}
                                                        onChange={this.handleChange}
                                                        required
                                                        id="department"
                                                >
                                                    <option value="" defaultValue>Choose...</option>
                                                    <option value="HR">HR</option>
                                                    <option value="Tech">Tech</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal"
                                                    onClick={this.dismissModal}>Close
                                            </button>
                                            <button type="submit" className="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }

}

export default AddInterviewerModal;
