import React from 'react';
import Interviewer from "./Interviewer";
import EditInterviewerModal from "./EditInterviewerModal";
import AddInterviewerModal from "./AddInterviewerModal";

class InterviewersPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            interviewerBeingEdited: undefined,
            interviewers: undefined,
            successMessage: undefined,
            errorMessage: undefined,
        }
    }

    componentDidMount() {
        fetch('http://localhost:8080/api/v1/interviewers')
            .then(response => response.json())
            .then(interviewers => this.setState({interviewers}));
    }

    toggleEditInterviewerModal = (interviewerId) => this.setState({
        showModal: !this.state.showModal,
        interviewerBeingEdited: this.getInterviewerById(interviewerId)
    })

    toggleAddInterviewerModal = () => this.setState({
        showModal: !this.state.showModal,
        interviewerBeingEdited: undefined
    })

    getInterviewerById(interviewerId) {
        if (this.state.interviewers !== undefined && interviewerId !== undefined) {
            return this.state.interviewers.find(interviewer => interviewer.id === interviewerId);
        }
        return undefined;
    }

    toggleFeedbackAlert = (message, type) => {
        switch (type) {
            case 'success':
                this.setState({
                    successMessage: message
                })
                break;
            case 'error':
                this.setState({
                    errorMessage: message
                })
                break;
        }
        this.componentDidMount()
    }

    deleteInterviewer(interviewerId) {
        fetch('http://localhost:8080/api/v1/interviewers/' + interviewerId, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    render() {
        return (
            <div className="clearfix">
                <button type="button" className="btn btn-outline-success my-2"
                        data-bs-toggle="modal" data-bs-target="#addInterviewerModal"
                        onClick={() => {
                            this.toggleAddInterviewerModal()
                        }}
                >
                    Add new interviewer
                </button>
                <AddInterviewerModal
                    toggle={this.toggleAddInterviewerModal}
                    showModal={this.state.showModal}
                    feedbackHandler={this.toggleFeedbackAlert}
                />
                {
                    this.state.successMessage !== undefined &&
                    <div className="alert alert-success" role="alert">
                        {this.state.successMessage}
                    </div>
                }
                {
                    this.state.errorMessage !== undefined &&
                    <div className="alert alert-danger" role="alert">
                        {this.state.errorMessage}
                    </div>
                }
                <div className="row row-cols-1 row-cols-md-2 g-4">
                    {
                        this.state.interviewers !== undefined &&
                        this.state.interviewers.map(
                            (interviewer) => {
                                return (
                                    <div className="col" key={interviewer.id}>
                                        <Interviewer key={interviewer.id}
                                                   id={interviewer.id}
                                                   fullName={interviewer.fullName}
                                                   email={interviewer.email}
                                                   department={interviewer.department}
                                                   createdAt={interviewer.createdAt}
                                                   updatedAt={interviewer.updatedAt}
                                                   deleteInterviewer={this.deleteInterviewer}
                                                   toggleModal={this.toggleEditInterviewerModal}
                                                   feedbackHandler={this.toggleFeedbackAlert}
                                        />
                                    </div>
                                )
                            }
                        )
                    }
                    {
                        this.state.interviewers !== undefined && this.state.interviewerBeingEdited !== undefined &&
                        <EditInterviewerModal
                            toggle={this.toggleEditInterviewerModal}
                            showModal={this.state.showModal}
                            interviewerBeingEdited={this.state.interviewerBeingEdited}
                            feedbackHandler={this.toggleFeedbackAlert}
                        />
                    }
                </div>
            </div>
        );
    }
}

export default InterviewersPage;
