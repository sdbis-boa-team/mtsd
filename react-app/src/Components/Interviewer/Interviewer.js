import React from 'react';

class Interviewer extends React.Component {
    render() {
        return (
            <li className="list-group-item list-group-item-action my-2">
                <div className="row">
                    <div className="col-10">
                        <p className="mb-1 fw-bold">{this.props.fullName}</p>
                        <img
                            src="interviewer.png"
                            alt={"Profile picture for " + this.props.fullName} className="m-2 float-start"/>
                        <p>Department: {this.props.department}</p>
                        <p>Contact: <a href={"mailto:" + this.props.email}>{this.props.email}</a>
                        </p>
                        <small><p className="mb-1">
                            {
                                "Added at " +
                                new Intl.DateTimeFormat("en-GB", {
                                    year: 'numeric',
                                    month: 'numeric',
                                    day: 'numeric',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    second: 'numeric'
                                }).format(Date.parse(this.props.createdAt))
                            }
                        </p></small>
                        <small><p className="mb-1">
                            {
                                "Updated at " +
                                new Intl.DateTimeFormat("en-GB", {
                                    year: 'numeric',
                                    month: 'numeric',
                                    day: 'numeric',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    second: 'numeric'
                                }).format(Date.parse(this.props.updatedAt))
                            }
                        </p></small>
                    </div>
                    <div className="col-2 d-inline justify-content-start">
                        <button type="button" className="btn btn-outline-primary mb-2 me-2"
                                data-bs-toggle="modal" data-bs-target="#editInterviewerModal"
                                onClick={() => {
                                    this.props.toggleModal(this.props.id)
                                }}>Edit
                        </button>
                        <button type="button" className="btn btn-outline-danger mb-2 me-2"
                                onClick={() => {
                                    if (window.confirm('Are you sure you want to delete ' + this.props.fullName + '?')) {
                                        this.props.feedbackHandler('Successfully deleted interviewer ' + this.props.fullName, 'success');
                                        this.props.deleteInterviewer(this.props.id);
                                    }
                                }}
                        >Delete
                        </button>
                    </div>
                </div>

            </li>
        );
    }
}

export default Interviewer;
