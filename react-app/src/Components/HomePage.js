import React from 'react';

class HomePage extends React.Component {
    render() {
        return (
            <div className="mt-3">
                <h3>Hello, dear visitor!</h3>
                <p>The purpose of our application is to facilitate the monitoring of employment steps as well as
                    possible.</p>
                <p>We are a team (BOA) made up of 3 skilled, balanced, persevering people, willing to work, who want to
                    help the hiring companies in finding the best employees.</p>
                <p>Take a look through the application features just to convince by yourself!</p>
                <p>Use our application and you will never have to worry about your interviews/employment management!</p>
            </div>
        );
    }
}

export default HomePage;
