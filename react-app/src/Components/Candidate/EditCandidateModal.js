import React from 'react';

class EditCandidateModal extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            ...this.props.candidateBeingEdited
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.candidateBeingEdited !== this.props.candidateBeingEdited) {
            this.setState({
                ...this.props.candidateBeingEdited
            })
        }
    }

    handleSubmit(e) {
        fetch('http://localhost:8080/api/v1/candidates/' + this.state.id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state),
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                this.props.feedbackHandler('Successfully updated candidate ' + this.state.fullName, 'success');
            })
            .catch((error) => {
                console.error('Error:', error);
                this.props.feedbackHandler('Error while trying to update candidate ' + this.state.fullName, 'error');
            });
        e.preventDefault();
        this.dismissModal();
    }

    handleChange = (e) => {
        const id = e.target.id;
        const value = e.target.value;
        this.setState({
            [id]: value
        });
    }

    dismissModal = () => {
        this.props.toggle(this.props.candidateBeingEdited.id);
    }

    render() {
        const candidateBeingEdited = this.state.id !== undefined;
        return (
            <div>
                {candidateBeingEdited &&
                <div
                    className={`modal fade editCandidateModal ${this.props.showModal ? 'show' : ''}`}
                    style={{
                        display: `${this.props.showModal ? 'block' : 'none'}`,
                        backgroundColor: `${this.props.showModal ? 'rgba(0,0,0,0.5)' : ''}`,
                    }} tabIndex="-1" id="editCandidateModal">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Update candidate with ID {this.state.id}</h5>
                                <button type="button" className="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"
                                        onClick={this.dismissModal}/>
                            </div>
                            <div className="modal-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row mb-3">
                                        <label htmlFor="fullName" className="col-sm-2 col-form-label">Name</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control" id="fullName"
                                                   value={this.state.fullName}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="gender" className="col-sm-2 col-form-label">Gender</label>
                                        <div className="col-sm-10">
                                            <select className="form-select" aria-label="Gender select"
                                                    value={this.state.gender}
                                                    onChange={this.handleChange}
                                                    required
                                                    id="gender"
                                            >
                                                <option value="female">Female</option>
                                                <option value="male">Male</option>
                                                <option value="other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="homeAddress"
                                               className="col-sm-2 col-form-label">Location</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control" id="homeAddress"
                                                   value={this.state.homeAddress}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="email" className="col-sm-2 col-form-label">Birthdate</label>
                                        <div className="col-sm-10">
                                            <input type="date" className="form-control" id="email"
                                                   value={new Date(this.state.birthDate).toISOString().slice(0, 10)}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                                        <div className="col-sm-10">
                                            <input type="email" className="form-control" id="email"
                                                   value={this.state.email}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="mobileNumber" className="col-sm-2 col-form-label">Mobile
                                            number</label>
                                        <div className="col-sm-10">
                                            <input type="tel" className="form-control" id="mobileNumber"
                                                   value={this.state.mobileNumber}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="workExperience" className="col-sm-2 col-form-label">Work
                                            experience (years)</label>
                                        <div className="col-sm-10">
                                            <input type="number" className="form-control" id="workExperience"
                                                   value={this.state.workExperience}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="studies" className="col-sm-2 col-form-label">Studies
                                            level</label>
                                        <div className="col-sm-10">
                                            <select className="form-select" aria-label="Studies level select"
                                                    value={this.state.studies}
                                                    onChange={this.handleChange}
                                                    required
                                                    id="studies"
                                            >
                                                <option value="highschool">Highschool diploma</option>
                                                <option value="bachelor">Bachelor's degree</option>
                                                <option value="master">Master's degree</option>
                                                <option value="phd">PhD</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <label htmlFor="mainSkills" className="col-sm-2 col-form-label">Main
                                            skills</label>
                                        <div className="col-sm-10">
                                            <input type="text" className="form-control" id="mainSkills"
                                                   value={this.state.mainSkills}
                                                   onChange={this.handleChange}
                                                   required
                                            />
                                        </div>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal"
                                                onClick={this.dismissModal}>Close
                                        </button>
                                        <button type="submit" className="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                }
            </div>
        );
    }
}

export default EditCandidateModal;
