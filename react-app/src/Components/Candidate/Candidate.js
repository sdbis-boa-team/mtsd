import React from 'react';

class Candidate extends React.Component {
    getAge(dateString) {
        const today = new Date();
        const birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    render() {
        return (
            <li className="list-group-item list-group-item-action my-2">
                <div className="row">
                    <div className="col-10">
                        <p className="mb-1 fw-bold">{this.props.fullName}</p>
                        <img
                            src={this.props.gender + "-candidate.png"}
                            alt={"Profile picture for " + this.props.fullName} className="m-2 float-start"/>
                        <p>Gender: {this.props.gender}</p>
                        <p>Location: {this.props.homeAddress}</p>
                        <p>Birthdate: {
                            new Intl.DateTimeFormat("en-GB", {
                                year: "numeric",
                                month: "long",
                                day: "2-digit"
                            }).format(Date.parse(this.props.birthDate))
                        } ({this.getAge(this.props.birthDate)})</p>
                        <p>Contact: <a href={"mailto:" + this.props.email}>{this.props.email}</a>, <a
                            href={"tel:" + this.props.mobileNumber}>{this.props.mobileNumber}</a>
                        </p>
                        <p>Work experience: {this.props.workExperience} years</p>
                        <p>Studies level: {this.props.studies}</p>
                        <p>Main skills: {this.props.mainSkills}</p>
                        <small><p className="mb-1">
                            {
                                "Added at " +
                                new Intl.DateTimeFormat("en-GB", {
                                    year: 'numeric',
                                    month: 'numeric',
                                    day: 'numeric',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    second: 'numeric'
                                }).format(Date.parse(this.props.createdAt))
                            }
                        </p></small>
                        <small><p className="mb-1">
                            {
                                "Updated at " +
                                new Intl.DateTimeFormat("en-GB", {
                                    year: 'numeric',
                                    month: 'numeric',
                                    day: 'numeric',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    second: 'numeric'
                                }).format(Date.parse(this.props.updatedAt))
                            }
                        </p></small>
                    </div>
                    <div className="col-2 d-inline justify-content-start">
                        <button type="button" className="btn btn-outline-primary mb-2 me-2"
                                data-bs-toggle="modal" data-bs-target="#editCandidateModal"
                                onClick={() => {
                                    this.props.toggleModal(this.props.id)
                                }}>Edit
                        </button>
                        <button type="button" className="btn btn-outline-danger mb-2 me-2"
                                onClick={() => {
                                    if (window.confirm('Are you sure you want to delete ' + this.props.fullName + '?')) {
                                        this.props.feedbackHandler('Successfully deleted candidate ' + this.props.fullName, 'success');
                                        this.props.deleteCandidate(this.props.id);
                                    }
                                }}
                        >Delete
                        </button>
                    </div>
                </div>

            </li>
        );
    }
}

export default Candidate;
