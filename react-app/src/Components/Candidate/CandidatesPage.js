import React from 'react';
import Candidate from "./Candidate";
import EditCandidateModal from "./EditCandidateModal";
import AddCandidateModal from "./AddCandidateModal";

class CandidatesPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            candidateBeingEdited: undefined,
            candidates: undefined,
            successMessage: undefined,
            errorMessage: undefined,
        }
    }

    componentDidMount() {
        fetch('http://localhost:8080/api/v1/candidates')
            .then(response => response.json())
            .then(candidates => this.setState({candidates}));
    }

    toggleEditCandidateModal = (candidateId) => this.setState({
        showModal: !this.state.showModal,
        candidateBeingEdited: this.getCandidateById(candidateId)
    })

    toggleAddCandidateModal = () => this.setState({
        showModal: !this.state.showModal,
        candidateBeingEdited: undefined
    })

    getCandidateById(candidateId) {
        if (this.state.candidates !== undefined && candidateId !== undefined) {
            return this.state.candidates.find(candidate => candidate.id === candidateId);
        }
        return undefined;
    }

    toggleFeedbackAlert = (message, type) => {
        switch (type) {
            case 'success':
                this.setState({
                    successMessage: message
                })
                break;
            case 'error':
                this.setState({
                    errorMessage: message
                })
                break;
        }
        this.componentDidMount()
    }

    deleteCandidate(candidateId) {
        fetch('http://localhost:8080/api/v1/candidates/' + candidateId, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    render() {
        return (
            <div className="clearfix">
                <button type="button" className="btn btn-outline-success my-2"
                        data-bs-toggle="modal" data-bs-target="#addCandidateModal"
                        onClick={() => {
                            this.toggleAddCandidateModal()
                        }}
                >
                    Add new candidate
                </button>
                <AddCandidateModal
                    toggle={this.toggleAddCandidateModal}
                    showModal={this.state.showModal}
                    feedbackHandler={this.toggleFeedbackAlert}
                />
                {
                    this.state.successMessage !== undefined &&
                    <div className="alert alert-success" role="alert">
                        {this.state.successMessage}
                    </div>
                }
                {
                    this.state.errorMessage !== undefined &&
                    <div className="alert alert-danger" role="alert">
                        {this.state.errorMessage}
                    </div>
                }
                <div className="row row-cols-1 row-cols-md-2 g-4">
                    {
                        this.state.candidates !== undefined &&
                        this.state.candidates.map(
                            (candidate) => {
                                return (
                                    <div className="col" key={candidate.id}>
                                        <Candidate key={candidate.id}
                                                   id={candidate.id}
                                                   fullName={candidate.fullName}
                                                   gender={candidate.gender}
                                                   birthDate={candidate.birthDate}
                                                   homeAddress={candidate.homeAddress}
                                                   email={candidate.email}
                                                   mobileNumber={candidate.mobileNumber}
                                                   workExperience={candidate.workExperience}
                                                   studies={candidate.studies}
                                                   mainSkills={candidate.mainSkills}
                                                   createdAt={candidate.createdAt}
                                                   updatedAt={candidate.updatedAt}
                                                   deleteCandidate={this.deleteCandidate}
                                                   toggleModal={this.toggleEditCandidateModal}
                                                   feedbackHandler={this.toggleFeedbackAlert}
                                        />
                                    </div>
                                )
                            }
                        )
                    }
                    {
                        this.state.candidates !== undefined && this.state.candidateBeingEdited !== undefined &&
                        <EditCandidateModal
                            toggle={this.toggleEditCandidateModal}
                            showModal={this.state.showModal}
                            candidateBeingEdited={this.state.candidateBeingEdited}
                            feedbackHandler={this.toggleFeedbackAlert}
                        />
                    }
                </div>
            </div>
        );
    }
}

export default CandidatesPage;
