import './App.css';
import React from 'react';
import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom';
import HomePage from "./Components/HomePage";
import CandidatesPage from "./Components/Candidate/CandidatesPage";
import InterviewersPage from "./Components/Interviewer/InterviewersPage";
import JobsPage from "./Components/JobsPage";
import ContactPage from "./Components/ContactPage";
import InterviewsPage from "./Components/InterviewsPage";
import TestsPage from "./Components/TestsPage";
import QuestionsPage from "./Components/QuestionsPage";

class App extends React.Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div className="container-fluid">
                            <a className="navbar-brand" href="/">Interviews Management</a>
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"/>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/" exact>Home</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/candidates">Candidates</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/jobs">Jobs</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/interviews">Interviews</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/interviewers">Interviewers</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/tests">Tests</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/questions">Questions</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/contact">Contact</NavLink>
                                    </li>
                                </ul>
                                <button className="btn btn-outline-success text-white" type="submit">Login</button>
                            </div>
                        </div>
                    </nav>
                    <div className="row">
                        <div className="col-2"/>
                        <div className="col-8">
                            <Switch>
                                <Route path="/" exact>
                                    <HomePage/>
                                </Route>
                                <Route path="/candidates">
                                    <CandidatesPage/>
                                </Route>
                                <Route path="/jobs">
                                    <JobsPage/>
                                </Route>
                                <Route path="/interviews">
                                    <InterviewsPage/>
                                </Route>
                                <Route path="/interviewers">
                                    <InterviewersPage/>
                                </Route>
                                <Route path="/tests">
                                    <TestsPage/>
                                </Route>
                                <Route path="/questions">
                                    <QuestionsPage/>
                                </Route>
                                <Route path="/contact">
                                    <ContactPage/>
                                </Route>
                            </Switch>
                        </div>
                        <div className="col-2"/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
